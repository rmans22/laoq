<?php

function getState()
{
    $connect = dbConnect();
    $sql = "SELECT * FROM state";
    $stmt = $connect->prepare($sql);
    $stmt->execute();

    return $stmt->get_result();
}

function getMemberTitle()
{
    $connect = dbConnect();
    $sql = "SELECT * FROM member_title";
    $stmt = $connect->prepare($sql);
    $stmt->execute();

    return $stmt->get_result();
}

function getSex()
{
    $connect = dbConnect();
    $sql = "SELECT * FROM member_sex ORDER BY sex ASC";
    $stmt = $connect->prepare($sql);
    $stmt->execute();

    return $stmt->get_result();
}


function getRoleDisplayImages($id)
{
    $connect = dbConnect();
    $query = "SELECT * FROM user_role WHERE id IN ($id)";
    $stms = $connect->prepare($query);
    $stms->execute();

    $result = $stms->get_result();

    while ($row = $result->fetch_field()) {
    }

    return $stms->get_result();
}


function getRoleList()
{
    $connect = dbConnect();
    $query = "SELECT * FROM user_role";
    $stms = $connect->prepare($query);
    $stms->execute();

    return $stms->get_result();
}


function getVisibaliyLabels($id)
{
    $connect = dbConnect();
    $query = "SELECT * FROM user_role WHERE id IN ($id)";
    $stms = $connect->prepare($query);
    $stms->execute();
    return $stms->get_result();
}
