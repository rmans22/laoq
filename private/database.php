<?php

function dbLoginInfo()
{
    return $dbConfig = [
        'host' => 'localhost',
        'username' => 'root',
        'password' => '',
        'dbname' => 'laoq'
    ];
}

function dbConnect()
{
    $dbConfig = dbLoginInfo();
    $connect = @new mysqli($dbConfig['host'], $dbConfig['username'], $dbConfig['password'], $dbConfig['dbname']);

    if ($connect->connect_errno) {
        die('Failed to connect to databse ' . $connect->connect_errno . ' ' . $connect->connect_error);
    }

    return $connect;
}



function escapeString($string)
{
    return mysqli_real_escape_string(dbConnect(), $string);
}