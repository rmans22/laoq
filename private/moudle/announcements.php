<?php

function createNewAnnouncement()
{
    echo 'hello world';
}

//Gets all active announcemetns.
function getAnnouncement()
{
    $connect = dbConnect();

    $query = "SELECT
    record_status.record_id,
    announcements.id AS 'annoc_id',
    announcements.user_id,
    announcements.title,
    announcements.text,
    member_contact_details.id,
    member_contact_details.firstname,
    member_contact_details.lastname,
    announcements_categories.title  AS 'cat_title',
    announcements_categories.id AS 'cat_id'
FROM
record_status
LEFT JOIN announcements ON announcements.id = record_status.record_id
LEFT JOIN member_contact_details ON member_contact_details.user_id = announcements.user_id
LEFT JOIN announcements_categories ON announcements_categories.id = announcements.category_id
WHERE
 record_status.is_active = 1";

    $stmt = $connect->prepare($query);
    $stmt->execute();
    return $stmt->get_result();
}


function getAnnouncementById($id)
{
    $connect = dbConnect();
    $query =  "SELECT * FROM announcements WHERE id=?";
    $stms = $connect->prepare($query);
    $stms->bind_param('i', $id);
    $stms->execute();

    return $stms->get_result();
}


function getAnnouncementRole()
{
    $connect = dbConnect();
    $query = "SELECT * FROM user_role";
    $stms = $connect->prepare($query);
    $stms->execute();
    return $stms->get_result();
}

function editCategoryLabel($id)
{
    $connect = dbConnect();
    $query = "SELECT * FROM user_role WHERE id IN ($id)";
    $stms = $connect->prepare($query);
    $stms->execute();
    return $stms->get_result();
}

//Gets all deleted announcements.
function getAllDeletedAnnouncement()
{
    $query = "SELECT
    record_status.record_id,
    record_status.in_trash,
    announcements.id,
    announcements.title,
    announcements.text,
    announcements.category_id,
    announcements.user_id,
    member_contact_details.firstname,
    member_contact_details.lastname,
    announcements.category_id,
    announcements_categories.title AS 'category_title',
    announcements.user_id AS 'member_id'
FROM
    record_status
    LEFT JOIN announcements ON announcements.id=record_status.record_id
    LEFT JOIN member_contact_details ON member_contact_details.user_id=announcements.user_id
    LEFT JOIN announcements_categories ON announcements_categories.id = announcements.category_id
    WHERE record_status.in_trash=?";

    $inTrash = 1;
    $connect = dbConnect();
    $stmt = $connect->prepare($query);
    $stmt->bind_param('i', $inTrash);
    $stmt->execute();

    return $stmt->get_result();
}

function geNumberOfRecordsInTrach()
{
    $query = "SELECT id, in_trash FROM record_status WHERE in_trash=?";

    $connect = dbConnect();
    $inTrash = 1;

    $smt = $connect->prepare($query);
    $smt->bind_param('i', $inTrash);
    $smt->execute();
    $result = $smt->get_result();

    echo json_encode([
        'success' => true,
        'message' => 'sfsaf',
        'show_message' => false,
        'trash_counter' => $result->num_rows
    ]);

    return $smt->get_result();
}

function sendAnnouncementToTrash($id)
{
    $query = "UPDATE `record_status` SET `is_active` = ?,  `in_trash` = ? WHERE `record_id` = ?";
    $connect = dbConnect();
    $stmt = $connect->prepare($query);

    $isActive = 0;
    $inTrash = 1;

    $stmt->bind_param('iii', $isActive, $inTrash, $id);
    $stmt->execute();

    echo json_encode([
        'success' => true,
        'show_message' => false,
    ]);
}

//Restore announcement in trash.
function resotreAnnouncement($id)
{
    $query = "UPDATE record_status SET is_active=? , in_trash=? WHERE record_id=?";
    $connect = dbConnect();
    $stmt = $connect->prepare($query);

    $inTrash = 0;
    $isActive = 1;

    if ($stmt = $connect->prepare($query)) {
        $stmt->bind_param('iii', $isActive, $inTrash, $id);
        $stmt->execute();
        echo json_encode(['success' => true, 'id' => $id]);
    }
}

/**
 * This function will Permanently Delete a announcement
 */
function deleteAnnouncement($id)
{
    $query = "DELETE
        announcements,
        record_status
    FROM
        announcements
    INNER JOIN record_status ON record_status.record_id = announcements.id
    WHERE
    announcements.id = ?";

    $connect = dbConnect();

    if ($stmt = $connect->prepare($query)) {
        echo json_encode(['success' => true]);
        $stmt->bind_param('i', $id);

        $stmt->execute();
        $stmt->close();
    }
}

function getNumberOfComments()
{
    $query = "SELECT
    announcements.id,
    announcement_comment.comment,
    announcement_comment.user_id
FROM
    announcements
    LEFT JOIN announcement_comment ON announcement_comment.announcement_id=announcements.id";
}
