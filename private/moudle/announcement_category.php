<?php

function createCategory()
{
    $connect = dbConnect();

    $userId = 126; // FOR TESTING.
    $visibilityId = [];
    $categoryName = filter_input(INPUT_POST, 'catName');
    $result = [];

    $visibleFor = $_POST['visibility'];
    // $memberRole = $_POST['role'];
    $memberRoleId = [];

    foreach ($visibleFor as $ids) {
        array_push($visibilityId, $ids);
    }

    // foreach ($memberRole as $roleId) {
    //     array_push($memberRoleId, $roleId);
    // }

    $newIds = implode(',', $visibilityId);
    $roleId = implode(',', $memberRoleId);

    $query = "INSERT INTO `announcements_categories` (`user_id`, `title`, `visibility_id`, `date`) VALUES (?, ?, ?, NOW())";

    if ($stmt = $connect->prepare($query)) {
        $stmt->bind_param('iss', $userId, $categoryName, $newIds);
        $stmt->execute();
        $result['message'] = 'New record created successfully';
        $result['record_id'] = $stmt->insert_id;
        $result['success'] = true;
        echo json_encode($result);
    } else {
        echo $connect->error;
    }
}

// Get a category based on a specified category id
function getCategory($id)
{
    $connect = dbConnect();

    $query = "SELECT
        announcements_categories.id,
        announcements_categories.title,
        announcements_categories.visibility_id,
        announcements_categories.allow_to_edit_category_user_id,
        member_contact_details.firstname,
        member_contact_details.lastname,
        member_contact_details.id AS 'member_id'
    FROM
        announcements_categories
    LEFT JOIN member_contact_details ON member_contact_details.user_id = announcements_categories.user_id WHERE announcements_categories.id=?";

    $stmt = $connect->prepare($query);
    $stmt->bind_param('i', $id);
    $stmt->execute();

    return $stmt->get_result();
}

function updateCategory($data)
{
    $connect = dbConnect();
    $query = "UPDATE announcements_categories SET 
        title=?, allow_to_edit_category_user_id=?,
        allow_to_edit_category_user_id=?
        WHERE id=?";
}

// Delete a category based on a specified category id
function delectCategory($id)
{
    $connect = dbConnect();
    $query = "DELETE FROM announcements_categories WHERE id=?";
    $result = [];

    if ($stmt = $connect->prepare($query)) {
        $stmt->bind_param('i', $id);
        $stmt->execute();
        $result['message'] = "Category Deleted";
        $result['success'] = true;
        $stmt->close();
        echo json_encode($result);
    } else {
        $result['message'] = $connect->error;
        $result['succes'] = false;
        echo json_encode($result);
    }
}

function getAnnouncementCategories()
{
    $connect = dbConnect();

    $query = "SELECT
    announcements_categories.id,
    announcements_categories.title,
    announcements_categories.visibility_id,
    announcements_categories.date,
    user_role.user_type,
    user_role.id AS role_id,
    announcements_categories.allow_to_edit_category_user_id
FROM
    announcements_categories
LEFT JOIN member_contact_details ON member_contact_details.user_id = announcements_categories.user_id
RIGHT JOIN user_role ON user_role.id WHERE user_role.id IN (announcements_categories.visibility_id) ORDER BY announcements_categories.title";

    $stmt = $connect->prepare($query);
    $stmt->execute();

    return $stmt->get_result();
}


/**
 * Gets all the category display names.
 */
function getCategoryDisplayNames()
{
    $connect = dbConnect();
    $query = "SELECT * FROM announcements_categories";
    $stmt = $connect->prepare($query);
    $stmt->execute();
    $result = $stmt->get_result();
    return $result;
}

/**
 * Gets all permission list.
 */
function getPermissionList()
{
    $connect = dbConnect();
    $query = "SELECT * FROM user_role ";
    $stms = $connect->prepare($query);
    $stms->execute();

    return $stms->get_result();
}

/**
 * visible for  / edit announcements selset list
 * $permissionId = []; Selected permission id/ids
 */
function permissionList($permissionId)
{
    $permissionList = getPermissionList();
    while ($row = $permissionList->fetch_assoc()) {
        $id = $row['id'];
        $name = $row['user_type'];
        if (in_array($id, $permissionId)) {
            echo "<option value='$id' selected='selected'>" . $name . "</option>";
        } else {
            echo "<option value='$id'>" . $name . "</option>";
        }
    }
}
