<?php

function createTrashRecord($recordId)
{


    $connect = dbConnect();
    $query = "INSERT INTO `trash` (`record_id`, `delete_date`) VALUES (?, NOW())";

    $result = [];

    if ($stmt = $connect->prepare($query)) {
        $stmt->bind_param('i', $recordId);
        $stmt->execute();
        $result['message'] = "creating new trash record";
        $result['success'] = true;
        $stmt->close();
        echo json_encode($result);
    } else {
        $result['message'] = $connect->error;
        $result['succes'] = false;
        echo json_encode($result);
    }
}

function recordExists($recordId)
{

    $id = 75;
    $result = [];
    $connect = dbConnect();

    $query = 'SELECT * FROM `trash` WHERE record_id=?';

    if ($stmt = $connect->prepare($query)) {
        $stmt->bind_param('s', $recordId);
        $stmt->execute();
    } else {
        $result['message'] = $connect->error;
        $result['success'] = false;
        echo json_encode($result);
        return;
    }



    $dbResult = $stmt->get_result();
    $rowCount = $stmt->num_rows;
    $foundRec = false;

    if ($dbResult->num_rows > 0) {
        $result['message'] = "Do nothing for now: " . $dbResult->num_rows;
        $result['success'] = false;
        $stmt->close();
        echo json_encode($result);
        return;
    }

    createTrashRecord($recordId);
}

function updateTrashRecord()
{
    $query = "UPDATE ";
    $connect = dbConnect();
}

function getDeletedAnnouncements()
{
    $query = "SELECT
    trash.record_id,
    trash.user_id AS 'tras_user_id',
    announcements.title,
    announcements.text,
    announcements.category_id,
    announcements.date,
    announcements.id,
    announcements.user_id,
    announcements_categories.title AS 'cat_title',
    member_contact_details.firstname,
    member_contact_details.lastname
FROM
    trash
LEFT JOIN announcements ON announcements.id = trash.record_id

LEFT JOIN announcements_categories ON announcements_categories.id = announcements.category_id
LEFT JOIN member_contact_details ON member_contact_details.user_id = announcements.user_id
WHERE
    trash.delete = 1
ORDER BY
    cat_title";

    $connect = dbConnect();
    $stmt = $connect->prepare($query);
    $stmt->execute();

    return $stmt->get_result();
}
