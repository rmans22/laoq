jQuery(document).ready(function () {

	jsLibraryLoader();
	displayCategory();

	createNewCategory();
	editAnnounceCategory();
	deleteCategory();
	displayCategoryList();
	getAnnouncements();
	createAnnouncement();

	sendToTrash();
	editAnnouncement();
	getDeletedAnnouncements();

	numberOfRecodInTrach();
	resotreRecord();
	DeletedAnnouncement();
});

function createAnnouncement() {
	jQuery('#accocumentPost').on('submit', function (e) {
		var data = new FormData(this);
		var url = 'http://localhost/laoq/public/ajax/create_announcement.php';

		jQuery.ajax({
			url: url,
			type: 'POST',
			data: data,
			contentType: false,
			cache: false,
			processData: false,
			success: function (data, textStatus, jqXHR) {
				console.log(data);
				getAnnouncements();
			},
		}).fail(function (data) {
		});
		e.preventDefault();
	});
}

function jsLibraryLoader() {

	// Date picker
	jQuery('#datepicker').datepicker({
		autoclose: true,
		format: 'yyyy-mm-dd',
	});

	jQuery('#exampleTbl').DataTable({
		orderCellsTop: true,
		fixedHeader: true
	});

	$('#summernote').summernote({
		height: 300,
		dialogsInBody: true,
		dialogsFade: true, // Add fade effect on dialogs
		codeviewFilter: false,
		codeviewIframeFilter: true
	});

	// select 2
	jQuery('.select2').select2({
		theme: "bootstrap4",
	});

	$('.role').select2({
		theme: "bootstrap4",
	});
}

function createNewCategory() {
	jQuery('#create_category').on('submit', function (e) {
		var data = new FormData(this);
		var url = 'http://localhost/laoq/public/ajax/create_category.php';

		jQuery.ajax({
			url: url,
			type: 'POST',
			data: data,
			contentType: false,
			cache: false,
			processData: false,
			success: function (data, textStatus, jqXHR) {
				var result = JSON.parse(data);
				console.log(data);
				if (result.success) {
					Swal.fire(
						'Success',
						result.message,
						'success'
					);

					//Reset form fileds.
					jQuery('#create_category').trigger("reset");
					jQuery('.select2').val(null).trigger('change');
					jQuery("#create_category").trigger("reset");

					//update category view table.
					displayCategory();

					//update category list.
					displayCategoryList();
				}
			},
		}).fail(function (data) {
			var getResponse = JSON.stringify(data);
			var msg = JSON.parse(getResponse);
			var serverMsg = JSON.parse(msg.responseText);
			Swal.fire(
				'Error',
				serverMsg.message,
				'error'
			)
		});
		e.preventDefault();
	});
}

function editAnnounceCategory() {

	jQuery('body').on('click', '.cat_info', function () {

		//category ID
		var categoryId = jQuery(this).data('id');

		jQuery.ajax({
			type: 'GET',
			url: 'http://localhost/laoq/public/ajax/edit_category.php',
			data: { 'cat_id': categoryId, 'action': 'edit' },
			success: function (data) {
				jQuery('.result').html(data);
			},
		});
	});


	jQuery('#updateCategory').on('submit', function (e) {
		var data = new FormData(this);
		jQuery.ajax({
			url: 'http://localhost/laoq/public/ajax/edit_category.php',
			type: 'POST',
			data: data,
			contentType: false,
			cache: false,
			processData: false,
			success: function (data) {
				console.log(data);
			},
		});
		e.preventDefault();
	});
}


function deleteCategory() {
	$('body').on('click', '#delete_cat', function () {
		var categoryId = jQuery(this).data('id');
		Swal.fire({
			title: 'Note',
			text: "Do you really want to delete this category ?",  //+ catName + ' ?',
			showClass: {
				popup: 'animated fadeInDown faster'
			},
			hideClass: {
				popup: 'animated fadeOutUp faster'
			},
			icon: 'warning',
			showCancelButton: true,
			confirmButtonColor: '#3085d6',
			cancelButtonColor: '#d33',
			confirmButtonText: 'Yes',
			cancelButtonText: 'No',

		}).then((result) => {
			if (result.value) {
				jQuery.ajax({
					type: 'GET',
					url: 'http://localhost/laoq/public/ajax/edit_category.php',
					data: { 'cat_id': categoryId, 'action': 'delete' },
					success: function (data) {
						var result = JSON.parse(data);
						if (result.success) {
							Swal.fire(
								'Deleted!',
								result.message,
								'success'
							);

							//update category view table.
							displayCategory();

							//update category list.
							displayCategoryList();

						} else {
							Swal.fire(
								'error',
								result.message,
								'error'
							);

						}

					},
				});
			}
		})
	});
}

//Get categories.
function displayCategory() {
	jQuery.ajax({
		url: 'http://localhost/laoq/public/ajax/category_view.php',
		method: 'GET',
		success: function (data) {
			var result = jQuery.parseJSON(data);
			if (result.success) {
				jQuery('.catgories').html(result.html);
			}
		}
	});
}

//Get category list.
function displayCategoryList() {
	jQuery.ajax({
		url: 'http://localhost/laoq/public/ajax/getCategoryDisplayName.php',
		method: 'GET',
		success: function (data) {
			jQuery('.showCategoryList').html(data);
		}
	});
}

function getAnnouncements() {
	jQuery.ajax({
		url: 'http://localhost/laoq/public/ajax/getAllAnnouncement.php',
		method: 'GET',
		success: function (data) {
			var result = jQuery.parseJSON(data);
			if (result.success) {
				jQuery('.annoc').html(result.html);
			} else {
				jQuery('.annoc').html(result.html);
			}
		}
	});
}

function sendToTrash(e) {
	$('body').on('click', '.trash', function (e) {
		var url = 'http://localhost/laoq/public/ajax/getAllAnnouncement.php';
		var postId = jQuery(this).data('id');
		console.log('CAT ID:' + postId);
		// doDelete(postId, url);
		jQuery.ajax({
			url: url,
			method: 'GET',
			data: { 'id': postId },
			success: function (data) {
				requestResult = JSON.parse(data);
				if (requestResult.success) {
					getAnnouncements();
					numberOfRecodInTrach();
					getDeletedAnnouncements();
				}
			}
		});

		// numberOfRecodInTrach();

		// jQuery('.card-body').load('#timeline');
		// getAnnouncements();
		e.preventDefault();
	});
}

function resotreRecord() {
	jQuery('body').on('click', '.restoreAnnouncement', function () {
		var endpoint = 'getDeletedAnnouncements.php';
		var recordId = jQuery(this).data('id');
		jQuery.ajax({
			type: 'GET',
			url: 'http://localhost/laoq/public/ajax/' + endpoint,
			data: { id: recordId, 'action': 'restore'},
			success: function (data) {
				requestResult = JSON.parse(data);
				console.log(requestResult);
				if (requestResult.success) {
					getAnnouncements();
					numberOfRecodInTrach();
					getDeletedAnnouncements();
				}
			},
		});
		// console.log(result);
	});
}

function editAnnouncement() {
	//edit_annoc
	jQuery('body').on('click', '.edit_annoc', function () {
		var endpoint = 'edit_annoc.php';
		var postId = jQuery(this).data('id');
		doEdit(postId, endpoint)
		// console.log('Post id:' + postId);
	});
}

function getDeletedAnnouncements() {
	jQuery.ajax({
		url: 'http://localhost/laoq/public/ajax/getDeletedAnnouncements.php',
		method: 'GET',
		success: function (data) {
			// console.log(data);
			var result = jQuery.parseJSON(data);
			if (result.success) {
				jQuery('.test').html(result.html);
			} else {
				jQuery('.test').html(result.html);
			}
		}
	});
}

function DeletedAnnouncement() {
	jQuery('body').on('click', '.deleteAnnouncement', function (e) {
		var url = 'http://localhost/laoq/public/ajax/getDeletedAnnouncements.php';
		var postId = jQuery(this).data('id');
		jQuery.ajax({
			url: url,
			method: 'GET',
			data: { 'id': postId, 'action': 'delete'},
			success: function (data) {
				console.log(data);
				requestResult = JSON.parse(data);

				if (requestResult.success) {
					getAnnouncements();
					numberOfRecodInTrach();
					getDeletedAnnouncements();
				}
			}
		});
		e.preventDefault();
	})
}

//Get number of records in trash.
function numberOfRecodInTrach() {
	var endPomt = 'updateTrashCounter.php';
	var request = makeRequest(endPomt);

	if (!request.trash_counter) {
		jQuery('.data').html(request.trash_counter);
	}
	jQuery('.data').html(request.trash_counter);


	// console.log(x.trash_counter);
}

/**********Gen**********/

function doDelete(recordId, url) {
	Swal.fire({
		title: 'Note',
		text: "Do you really want to delete this post ?",
		showClass: {
			popup: 'animated fadeInDown faster'
		},
		hideClass: {
			popup: 'animated fadeOutUp faster'
		},
		icon: 'warning',
		showCancelButton: true,
		confirmButtonColor: '#3085d6',
		cancelButtonColor: '#d33',
		confirmButtonText: 'Yes',
		cancelButtonText: 'No',

	}).then((result) => {
		if (result.value) {
			jQuery.ajax({
				type: 'GET',
				url: url,
				data: { 'id': recordId },
				success: function (data) {
					console.log(data);
					// console.log(data);
					var result = JSON.parse(data);
					console.log(data);
					if (result.success) {

						if (result.show_message) {
							Swal.fire(
								'Deleted!',
								result.message,
								'success'
							);
						}


						getAnnouncements();
						getDeletedAnnouncements();
						numberOfRecodInTrach();
						location.reload();
					} else {
						Swal.fire(
							'error',
							result.message,
							'error'
						);
					}
				},
			});
		}
	})

}

function doUpdate(recordId) {


}

function doEdit(recordId, endpoint) {
	jQuery.ajax({
		type: 'GET',
		url: 'http://localhost/laoq/public/ajax/' + endpoint,
		data: { 'id': recordId, 'action': 'edit' },
		success: function (data) {
			jQuery('.annoc_result').html(data);
			console.log('Result: ' + data);
		},
	});
}

function makeRequest(endpoint) {
	var requestResult = '';
	jQuery.ajax({
		type: 'GET',
		url: 'http://localhost/laoq/public/ajax/' + endpoint,
		data: {},
		async: false,
		success: function (data) {
			requestResult = JSON.parse(data);;
		},
	});

	return requestResult;
}

//https://www.mysqltutorial.org/mysql-delete-join/