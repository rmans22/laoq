<?php
require_once '../inc/init.php';
$categoryNames = getAnnouncementCategories();

$html = '<table class="table table-hover text-nowrap">
        <thead>
            <tr>
            <th width="25%">Title</th>
            <th width="25%">Visible for</th>
            <th width="32%">Edit announcements</th>
            <th width="25%">Actions</th>
            </tr>
        </thead>
        <tbody>';

while ($row = $categoryNames->fetch_assoc()) {

    $id = $row['id'];
    $visibility  = $row['visibility_id'];
    $categoryTitle = $row['title'];
    $editCategoryId = $row['allow_to_edit_category_user_id'];
    $date = $row['date'];

    $html .= '<tr>';
    $html .= '<td>';
    $html .= $row['title'];
    $html .= '</td>';

    $html .= '<td>';
    $getVisibility = getVisibaliyLabels($visibility);

    while ($row = $getVisibility->fetch_assoc()) {
        $img = $row['img'];
        $title = $row['user_type'];
        $html .= '<li class="list-inline-item">';
        $html .=  "<span class='badge badge bg-success'> " . $title . " <span>";
        $html .= '</li>';
    }
    $html .= '</td>';

    $html .= '<td>';
    $html .= $row['allow_to_edit_category_user_id'];
    $html .= '</td>';

    $html .= "<td class=''>
            
            <a class=\"btn btn-info btn-sm cat_info\" href='' data-toggle=\"modal\" data-target=\"#RecBackdrop\" data-id=\"$id\"
                  data-t=\"$visibility\">
                  <i class=\"fas fa-pencil-alt\"></i>
                      Edit
                  </a>

                  <button class=\"btn btn-danger btn-sm \"  data-id=\"$id\" id='delete_cat'>
                  <i class=\"fas fa-trash\">
                  </i>
                  Delete Permanently
              </button>
        </td>";
    $html .= '</tr>';
}


$html .= '</tbody>';
$html .= '</table>';
echo json_encode(['success' => true, 'html' => $html]);
