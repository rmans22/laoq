<?php

$cagegoryName = trim(filter_input(INPUT_POST, 'catName'));

$result = [];

if (empty($cagegoryName)) {
    $result['message'] = 'Field <b>Category</b> name is empty';
    $result['success'] =  false;
} elseif (!isset($_POST['visibility'])) {
    $result['message'] = 'Field <b>Visible for</b> is empty';
    $result['success'] =  false;
} else {
    $result['success'] = true;
}


switch ($result['success']) {
    case true:
        require_once '../inc/init.php';
        createCategory();
        break;
    case false:
        header('HTTP/1.0 500 Internal Server Error');
        echo json_encode($result);
        break;
}
