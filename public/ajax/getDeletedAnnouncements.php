<?php
require_once '../inc/init.php';

if (isset($_GET['action'])) {
    switch ($_GET['action']) {
        case 'restore':
            resotreAnnouncement($_GET['id']);
            return;
            break;
        case 'delete':
            deleteAnnouncement($_GET['id']);
            break;
    }
    return;
}

$demo = '';
$announcements = getAllDeletedAnnouncement();

if ($announcements->num_rows === 0) {
    $html = '<div class="alert alert-warning alert-dismisssible">
    <h4><i class="icon fas fa-exclamation-triangle"></i></h4>
    <p>No announcements found in Trash.</p>
  </div>';
    echo json_encode(['success' => false, 'html' => $html]);
    return;
}

$html = '<table class="table table-hover text-nowrap">
        <thead>
            <tr>
                <th width="25%">Title</th>
                <th width="25%">Author</th>
                <th width="32%">Categories</th>
                <th width="25%"><i class="fas fa-comment fa-lg"></i></th>
                <th></th>
            </tr>
        </thead>
        <tbody>';

while ($row = $announcements->fetch_assoc()) {

    $id = $row['id'];
    $title = $row['title'];

    $html .= '<tr>';
    $html .= '<td>';
    $html .= '<a href="#">' . $title . '</a>';
    $html .= '</td>';


    $html .= '<td>';
    $html .= $row['firstname'] . ' ' . $row['lastname'];
    $html .= '</td>';

    $html .= '<td>';
    $html .= $row['category_title'];
    $html .= '</td>';

    $html .= '<td>';
    $html .= '<span class="badge badge-primary right">6</span>';
    $html .= '</td>';

    $html .= "<td class=''>
   

                  <button class=\"btn btn-primary btn-sm  restoreAnnouncement\"  data-id=\"$id\">
                  <i class=\"fas fa-trash-restore\">
                  </i>
                  Restore
              </button>

              <button class=\"btn btn-danger btn-sm  deleteAnnouncement\"  data-id=\"$id\">
              <i class=\"fas fa-trash\">
              </i>
              Delete Permanently
          </button>
        </td>";
    $html .= '</tr>';
}


$html .= '</tbody>';
$html .= '</table>';
echo json_encode(['success' => true, 'html' => $html]);
