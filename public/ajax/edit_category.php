<?php

require_once '../inc/init.php';

if (!isset($_GET['cat_id']) || !is_numeric($_GET['cat_id'])) {
    return;
}

switch ($_GET['action']) {
    case 'edit':
        $category = getCategory($_GET['cat_id']);
        $result = $category->fetch_assoc();
        $categoryName = $result['title'];
        $visibleFor = $result['visibility_id'];
        $createdBy = 'riomansaray'; //$result['firstname'] . ' ' . $result['lastname'];
        $whoCanEditCategory = $result['allow_to_edit_category_user_id'];
        break;
    case 'delete':
        delectCategory($_GET['cat_id']);
        return;
        break;
}
?>

<?php
$visibleForIds = explode(',', $visibleFor);
$editAnnouncementId = explode(',', $whoCanEditCategory);
?>

<div class="form-group">
    <label class="col-form-label">Name</label>
    <input type="text" class="form-control" placeholder="Categroy name" value="<?php echo $categoryName; ?>">
</div>
<div class="form-group">
    <label class="col-form-label">Visible for</label>
    <select class="form-control customSelect" style="width: 100%;" multiple="multiple" name="visibility[]">
        <?php
        permissionList($visibleForIds);
        ?>
    </select>
</div>

<div class="form-group">
    <label class="col-form-label">Edit announcements	</label>
    <select class="form-control customSelect" style="width: 100%;" multiple="multiple" name="editAnnouncement[]">
        <?php
        permissionList($editAnnouncementId);
        ?>
    </select>
</div>

<script>
    jQuery('.customSelect').select2({
        theme: "bootstrap4",
        width: '100%',
        dropdownParent: jQuery("#RecBackdrop")
    })
</script>