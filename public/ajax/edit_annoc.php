<?php

require_once '../inc/init.php';

if (!isset($_GET['id'])) {
    return;
}

$id = $_GET['id'];
$announcement = getAnnouncementById($id);
// $categoryList = getCategoryDisplayNames();

$result = $announcement->fetch_assoc();

$id = $result['id'];
$title = $result['title'];
$text = $result['text'];
// $categoryId = implode(',', $result['category_id']);
?>

<div class="tab-pane" id="timeline">
    <form id="accocumentPost" method="POST">
        <div class="form-group">
            <input class="form-control" placeholder="Title" name="title" value="<?php echo $title; ?>">
        </div>

        <div class="form-group showCategoryList"></div>
        <textarea class="summernote" name="message"><?php echo $text; ?></textarea>
        <br>
        <!-- <button type="submit" class="btn btn-success float-left"><i class="fas fa-save"></i> Save</button> -->
    </form>
</div>

<script>
    $('.summernote').summernote({
        height: 300,
        dialogsInBody: true,
        dialogsFade: true, // Add fade effect on dialogs
        codeviewFilter: false,
        codeviewIframeFilter: true
    });

</script>