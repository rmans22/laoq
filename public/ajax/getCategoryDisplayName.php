<?php
require_once '../inc/init.php';
$result = getCategoryDisplayNames();
?>

<select class="form-control tt" style="width: 100%;" name="categoryName">
    <?php
    if ($result->num_rows > 0) {
        while ($row = $result->fetch_assoc()) {
            $categoryId = $row['id'];
            $categoryTtitle = $row['title'];
            echo "<option value='$categoryId'>" . $categoryTtitle . "</option>";
        }
    }
    ?>
</select>

<script>
    jQuery('.tt').select2({
        theme: "bootstrap4",
        width: '100%'
    })
</script>