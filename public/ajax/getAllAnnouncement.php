<?php
require_once '../inc/init.php';

if (isset($_GET['id'])) {
    sendAnnouncementToTrash($_GET['id']);
    return;
}

$announcements = getAnnouncement();

if ($announcements->num_rows === 0) {
    $html = '<div class="alert alert-info alert-dismissible">
    <h5><i class="icon fas fa-info"></i> Alert!</h5>
    No announcements found.
  </div>';
    echo json_encode(['success' => false, 'html' => $html]);
    return;
}

$html = '<table class="table table-hover text-nowrap">
        <thead>
            <tr>
                <th width="25%">Title</th>
                <th width="25%">Author</th>
                <th width="32%">Categories</th>
                <th width="25%"><i class="fas fa-comment fa-lg"></i></th>
                <th></th>
            </tr>
        </thead>
        <tbody>';

while ($row = $announcements->fetch_assoc()) {

    $id = $row['annoc_id'];
    $title = $row['title'];
    $categoryId = $row['cat_id'];

    $html .= '<tr>';
    $html .= '<td>';
    $html .= '<a href="#">' . $title . '</a>';
    $html .= '</td>';


    $html .= '<td>';
    $html .= $row['firstname'] . ' ' . $row['lastname'];
    $html .= '</td>';

    $html .= '<td>';
    $html .= $row['cat_title'];
    $html .= '</td>';

    $html .= '<td>';
    $html .= '<span class="badge badge-primary right">6</span>';
    $html .= '</td>';

    $html .= "<td class=''>
    <a class='btn btn-primary btn-sm' href='#'>
                              <i class='fas fa-folder'>
                              </i>
                              View
                          </a>
            
            <a class=\"btn btn-info btn-sm edit_annoc\" href='' data-toggle=\"modal\" data-target=\"#anncoModal\" data-id=\"$categoryId\">
                  <i class=\"fas fa-pencil-alt\"></i>
                      Edit
                  </a>

                  <button class=\"btn btn-danger btn-sm  trash\"  data-id=\"$id\">
                  <i class='far fa-trash-alt'></i>
                  Trash
              </button>
        </td>";
    $html .= '</tr>';
}


$html .= '</tbody>';
$html .= '</table>';
echo json_encode(['success' => true, 'html' => $html]);
