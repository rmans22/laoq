<?php
$pageTitle = 'Announcements';
include '../inc/header.php';
?>
<div class="col-md-30">
    <div class="card">
        <div class="card-header p-2">
            <ul class="nav nav-pills">
                <li class="nav-item"><a class="nav-link active" href="#activity" data-toggle="tab"> <i class="fa fa-newspaper"></i> Announcements</a></li>
                <li class="nav-item"><a class="nav-link" href="#timeline" data-toggle="tab"><i class="fas fa-scroll"></i> Add New</a></li>
                <li class="nav-item"><a class="nav-link" href="#settings" data-toggle="tab"><i class="fas fa-list-alt"></i> Categories</a></li>
                <li class="nav-item"><a class="nav-link" href="#createCategory" data-toggle="tab"><i class="far fa-list-alt"></i> Create Category</a></li>
                <li class="nav-item"><a class="nav-link" href="#trash" data-toggle="tab"><i class="far fa-trash-alt"></i> Trash <span class="badge badge-danger right data"></span></a></li>
                <?php

                $deletedAnnouncements = getAllDeletedAnnouncement();
                $roleList = getRoleList();

                if ($deletedAnnouncements->num_rows > 0) {
                    $deletedAnnouncementsCounter = $deletedAnnouncements->num_rows;
                }
                ?>
            </ul>
        </div>
        <div class="card-body">
            <div class="tab-content">

                <div class="tab-pane active" id="activity">
                    <div class="annoc"></div>
                </div>

                <!-- /.tab-pane -->

                <div class="tab-pane" id="timeline">
                    <form id="accocumentPost" method="POST">
                        <div class="form-group">
                            <input class="form-control" placeholder="Title" name="title">
                        </div>

                        <div class="form-group showCategoryList"></div>
                        <textarea id="summernote" name="message"></textarea>

                        <button type="submit" class="btn btn-success float-left">Publish</button>
                    </form>
                </div>
                <!-- /.tab-pane -->

                <div class="tab-pane " id="settings">
                    <button type="button" class="btn btn-info float-left"><i class="fas fa-plus"></i> Add New Category</button>
                    <br /><br />
                    <div class="catgories"></div>
                </div>

                <div class="tab-pane" id="createCategory">
                    <form class="form-horizontal" id="create_category">
                        <div class="form-group row">
                            <label for="catName" class="col-sm-2 col-form-label">Category Name</label>
                            <div class="col-sm-10">
                                <input type="text" class="form-control" id="catName" name="catName" required />
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="inputEmail" class="col-sm-2 col-form-label">Visible for</label>
                            <div class="col-sm-10">
                                <select class="form-control select2" style="width: 100%;" multiple="multiple" name="visibility[]">
                                    <?php
                                    while ($row = $roleList->fetch_assoc()) {
                                        $id = $row['id'];
                                        echo "<option value='$id'>" . $row['user_type'] . "</option>";
                                    }
                                    ?>
                                </select>
                            </div>
                        </div>

                        <div class="form-group row">
                            <div class="offset-sm-2 col-sm-10">
                                <button type="submit" class="btn btn-success float-left">Add New Category</button>
                            </div>
                        </div>
                    </form>

                </div>

                <div class="tab-pane" id="trash">

                    <div class="test"></div>

                </div>


            </div>
            <!-- /.tab-pane -->
        </div>
        <!-- /.tab-content -->
    </div>
</div>
</div>
<br /><br />

<div class="modal fade" id="RecBackdrop" tabindex="-1" role="dialog" aria-labelledby="staticBackdrop" aria-hidden="true">
    <div class="modal-dialog modal-m" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="demo">Announcements Category</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <form class="form-horizontal" id="updateCategory">
                <div class="modal-body result">
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-primary">Save changes</button>
            </form>
        </div>
    </div>
</div>
</div>


<div class="modal fade" id="anncoModal" tabindex="-1" role="dialog" aria-labelledby="staticBackdrop" aria-hidden="true">
    <div class="modal-dialog modal-xl" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="demo">Edit - Announcement</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <form class="form-horizontal" id="updateCategory">
                <div class="modal-body annoc_result">
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-primary">Save changes</button>
            </form>
        </div>
    </div>
</div>
</div>

<?php
include '../inc/footer.php';
